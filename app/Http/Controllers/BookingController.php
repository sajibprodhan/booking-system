<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use Illuminate\Http\Request;

class BookingController extends Controller {

    public function availability() {
        $availableSlots = Booking::where('booking_date', '>=', now()->format('Y-m-d'))
            ->orderBy('booking_date')
            ->orderBy('start_time')
            ->get();
        return view('welcome', compact('availableSlots'));
    }

    public function store(Request $request) {
        $request->validate([
            'booking_name' => 'required|string',
            'booking_date' => 'required|date',
            'start_time'   => 'required|date_format:H:i',
            'end_time'     => 'required|date_format:H:i|after:start_time',
        ]);

        $existingBooking = Booking::firstWhere('booking_date', $request->booking_date,
            fn($query) =>
            $query->where('start_time', '<=', $request->start_time)
                ->where('end_time', '>=', $request->start_time)
                ->orWhere('start_time', '<=', $request->end_time)
                ->where('end_time', '>=', $request->end_time)
        );

        if ($existingBooking) {
            return back()->with('error', 'Slot not available for the selected date and time.');
        }

        $booking = new Booking([
            'booking_name' => $request->booking_name,
            'booking_date' => $request->booking_date,
            'start_time'   => $request->start_time,
            'end_time'     => $request->end_time,
        ]);

        $booking->save();

        return back()->withSuccess('Booking successful!!');
    }

}
