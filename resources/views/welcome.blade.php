<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap demo</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    </head>
    <body>

        <div class="container">
            <div class="col-sm-7 m-auto">
                <div class="row pt-4 px-0">

                    @if(session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul class="list-unstyled m-0 p-0">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <h3 class="m-0 p-0">Booking From</h3>
           
                    <form class="g-3 border p-4" method="post" action="{{ route('bookings.store') }}">
                        @csrf
                        <div class="col-12 mb-3">
                            <label class="form-label" for="booking_name">Booking/Meeting Name:</label>
                            <input id="booking_name" class="form-control" type="text" name="booking_name" value="{{ old('booking_name') }}" required>
                        </div>

                        <div class="col-12 mb-3">
                            <label class="form-label" for="booking_date">Date:</label>
                            <input id="booking_date" class="form-control" type="date" name="booking_date" value="{{ old('booking_date') }}" required>
                        </div>

                        <div class="col-12 mb-3">
                            <label class="form-label" for="start_time">Start Time:</label>
                            <input id="start_time" class="form-control" type="time" name="start_time" value="{{ old('start_time') }}" required>
                        </div>
                        <div class="col-12 mb-3">
                            <label class="form-label" for="end_time">End Time:</label>
                            <input id="end_time" class="form-control" type="time" name="end_time" value="{{ old('end_time') }}" required>
                        </div>
                        <div class="col-12 mb-3">
                            <button type="submit" class="btn btn-primary">Sign in</button>
                        </div>
                    </form>

                 </div>
            </div>
        </div>


        <div class="container mt-5">
            <div class="col-sm-7 m-auto">
                <div class="row">
                    <h3 class="m-0 p-0 pb-2">Available Slots</h3>
                    <ul class="list-unstyled m-0 p-0">
                        @forelse ($availableSlots as $slot)
                            <li class="mb-1">{{ $slot->booking_name }} - {{ $slot->booking_date }} - {{ $slot->start_time }} to {{ $slot->end_time }}</li>
                        @empty
                            <li>Slot Not Found</li>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>


        














           
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
    </body>
</html>